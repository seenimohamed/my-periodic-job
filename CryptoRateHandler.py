from __future__ import print_function
from apiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
from oauth2client.client import GoogleCredentials
import json
import httplib2
import sys

class CryptoRateCollector:

	def __init__(self) :
		self.http = httplib2.Http()

		self.kurl = "https://koinex.in/api/ticker"
# 		self.kcoins = ["NCASH","IOST","TRX","ZIL","BAT","XRP"]
		self.kcoins = ["ZIL","IOST"]

		self.burl = "https://bitbns.com/order/getTickerWithVolume/"
# 		self.bcoins = ["SC","ZIL"]
		self.bcoins = ["TRX", "XRP"]

	def getKoinexData(self) :
# 		content = self.http.request(self.kurl, method="GET")[1]
# 		priceinfo = json.loads(content)['prices']['inr']
		koinexresult = {}
# 		for coin in self.kcoins :
# 			koinexresult[coin] = float(priceinfo[coin].encode("utf-8"))
		return koinexresult

	def getBitbnsData(self) :
		content = self.http.request(self.burl, method="GET")[1]
		priceinfo = json.loads(content)
		bitbnsresult = {}
		for coin in self.bcoins :
			bitbnsresult[coin] = float(priceinfo[coin]['last_traded_price'])
		return bitbnsresult

	def getCryptoRates(self) :
		return self.getKoinexData(),self.getBitbnsData()

class GSheetsUpdater:

	def __init__(self,cratecollector,sheetname,sheetid,cred) :
		self.cratecollector = cratecollector
		# self.coins = coins
# 		self.kcoinrange = sheetname+'!B5'
# 		self.kraterange = sheetname+'!F5'

		self.bcoinrange = sheetname+'!B13'
		self.braterange = sheetname+'!F13'

		self.sheetid = sheetid
		self.cred = json.load(cred)

	def get2darray(self, crptoresult, coins) :
		coinvalues = map(lambda x: [x.lower()], coins)
		cratevalues = map(lambda x: [crptoresult[x]], coins)
		return coinvalues, cratevalues

	def constructbody(self, koinexresult, bitbnsresult) :

# 		kcoins, krates = self.get2darray(koinexresult, self.cratecollector.kcoins)
		bcoins, brates = self.get2darray(bitbnsresult, self.cratecollector.bcoins)

		data = [
# 			{
# 				'range' : self.kcoinrange,
# 				'values' : kcoins
# 			},
# 			{
# 				'range' : self.kraterange,
# 				'values' : krates
# 			},
			{
				'range' : self.bcoinrange,
				'values' : bcoins
			},
			{
				'range' : self.braterange,
				'values' : brates
			}
		]

		body = {
			'valueInputOption' : 'RAW',
			'data' : data
		}
		return body


	def loadservice(self) :
		creds = GoogleCredentials(
		    self.cred['access_token'],
		    self.cred['client_id'],
		    self.cred['client_secret'],
		    self.cred['refresh_token'],
		    self.cred['token_expiry'],
		    self.cred['token_uri'],
		    self.cred['user_agent'],
		    revoke_uri=self.cred['revoke_uri']
		    )

		SCOPES = 'https://www.googleapis.com/auth/spreadsheets'
		if not creds or creds.invalid:
		    flow = client.flow_from_clientsecrets('client_secret.json', SCOPES)
		    creds = tools.run_flow(flow, store)
		service = build('sheets', 'v4', http=creds.authorize(Http()))
		return service

	def updateinsheets(self, body) :
		service = self.loadservice()
		result = service.spreadsheets().values().batchUpdate(
			spreadsheetId=self.sheetid, body=body
			).execute()
		print('\t{0} cells updated.'.format(result.get('totalUpdatedCells')));


class IFTTTPusher :

	def __init__(self, ifttt_key, koinexdata, bitbnsdata,cratecollector) :
		self.ifttt_url = 'https://maker.ifttt.com/trigger/xrp_rate_trigger/with/key/'+ifttt_key
		self.koinexdata = koinexdata
		self.bitbnsdata = bitbnsdata
		self.cratecollector = cratecollector
		self.ifttt_result = ''

	def converttoiftttresult(self) :
		self.ifttt_result = 'Bitbns results\n'
# 		for coin in self.cratecollector.kcoins :
# 			self.ifttt_result = self.ifttt_result + coin +" = "+str(self.koinexdata[coin])+"\n";

# 		self.ifttt_result = self.ifttt_result + '\nBitbns results\n'
        # self.ifttt_result = 'Bitbns results\n'
		for coin in self.cratecollector.bcoins :
			self.ifttt_result = self.ifttt_result + coin +" = "+str(self.bitbnsdata[coin])+"\n";

	def pushtopushbullet(self) :
		data = {"value1":self.ifttt_result.strip()}
		resp, content = httplib2.Http().request(
	        uri=self.ifttt_url,
	        method='POST',
	        headers={'Content-Type': 'application/json; charset=UTF-8'},
	        body=json.dumps(data),
    	)
		return content


if __name__ == '__main__' :
	cratecollector = CryptoRateCollector()
	koinexdata,bitbnsdata = cratecollector.getCryptoRates()
	print('Data collected from crpto sites.')

	sheetid = sys.argv[1]
	cred = sys.stdin

	updater = GSheetsUpdater(cratecollector,'koinex',sheetid,cred)
	body = updater.constructbody(koinexdata,bitbnsdata)
	updater.updateinsheets(body)
	print('Made changes in gsheet.')

	ifttt_key = sys.argv[2]
	iftttpusher = IFTTTPusher(ifttt_key,koinexdata,bitbnsdata,cratecollector)
	iftttpusher.converttoiftttresult()
	result = iftttpusher.pushtopushbullet()
	print(result)
